import pandas as pd
import csv
from datetime import datetime

CNPJS = []
cnpj_results = []
cnpj_not_found = []
contador = 0

df = pd.read_excel('CNPJ\'S_SIMPLES_MEI.xlsx', usecols=[0], skiprows=1)
for i in range(len(df)):
    cnpj_to_str = (str(df['CNPJ\'S'][i]))

    if len(cnpj_to_str) < 14:
        diff = 14 - len(cnpj_to_str)
        CNPJS.append(diff * "0" + cnpj_to_str)
    else:
        CNPJS.append(cnpj_to_str)

CNPJS = list(dict.fromkeys(CNPJS))


def change_to_datetime(data_input, index):
    if ("00000000" in data_input[index].strip()):
        return ""
    else:
        return datetime.strptime(
            data_input[index].strip(), "%Y%m%d")


f = open("dados_simples.txt", "r")
Lines = f.readlines()

for cnpj in CNPJS:
    contador += 1
    print(f"{contador}/{len(CNPJS)}")

    dados_receita = [elem for elem in Lines if cnpj[0:8] in elem]

    if(len(dados_receita) != 0):
        dados_separados = dados_receita[0].replace('"', "").split(";")

        base_cnpj = dados_separados[0].strip()

        opcao_simples = dados_separados[1].strip()

        data_opcao_simples = change_to_datetime(dados_separados, 2)

        data_exclusao_simples = change_to_datetime(dados_separados, 3)

        opcao_mei = dados_separados[4].strip()

        data_opcao_mei = change_to_datetime(dados_separados, 5)

        data_exclusao_mei = change_to_datetime(dados_separados, 6)

        cnpj_resumo = {
            "cnpj": cnpj,
            "opcao_simple": opcao_simples,
            "data_opcao_simples": data_opcao_simples,
            "data_exclusao_simples": data_exclusao_simples,
            "opcao_mei": opcao_mei,
            "data_opcao_mei": data_opcao_mei,
            "data_exclusao_mei": data_exclusao_mei
        }
        cnpj_results.append(cnpj_resumo)
    else:
        cnpj_not_found.append(cnpj)
        print(f"{cnpj} não encontrado")

keys = cnpj_results[0].keys()
with open('cnpjs_infos.csv', 'w', newline='') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(cnpj_results)

print(cnpj_results, cnpj_not_found)
