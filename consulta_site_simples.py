import pandas as pd
import time
import csv
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from anticaptchaofficial.recaptchav2proxyless import *


def get_recaptcha_token():
    solver = recaptchaV2Proxyless()
    solver.set_verbose(1)
    solver.set_key("b2f7941460fdb717dd4477e4e24804bb")
    solver.set_website_url(
        "https://www8.receita.fazenda.gov.br/SimplesNacional/aplicacoes.aspx?id=21")

    solver.set_website_key("6LcdHekUAAAAAJ3tTUNYdDFJ2JhE2KxWS-wSm75A")

    g_response = solver.solve_and_return_solution()
    if g_response != 0:
        print("g-response: "+g_response)
        return g_response
    else:
        print("task finished with error "+solver.error_code)


PATH = "C:\Program Files (x86)\chromedriver.exe"
URL = "https://www8.receita.fazenda.gov.br/SimplesNacional/aplicacoes.aspx?id=21"

CNPJS = []
cnpj_results = []

df = pd.read_excel('CNPJ\'S_SIMPLES_MEI.xlsx', usecols=[0], skiprows=1)
for i in range(len(df)):
    cnpj_to_str = (str(df['CNPJ\'S'][i]))

    if len(cnpj_to_str) < 14:
        diff = 14 - len(cnpj_to_str)
        CNPJS.append(diff * "0" + cnpj_to_str)
    else:
        CNPJS.append(cnpj_to_str)


driver = webdriver.Chrome(PATH)
i = 0
for cnpj in CNPJS:
    driver.get(URL)
    driver.switch_to.frame("frame")
    input_element = driver.find_elements_by_class_name("form-control")
    input_element[0].send_keys(cnpj)
    token_recaptcha = get_recaptcha_token()
    driver.execute_script(
        f"$('#tokenRecapcha').attr('value', {token_recaptcha})")
    driver.execute_script('document.getElementById("consultarForm").submit()')
    # driver.find_element_by_id("btnSubmit").click()
    # try:

    #     WebDriverWait(driver, 10).until(
    #         ec.presence_of_element_located((By.CLASS_NAME, "panel-body")))

    # except:
    #     driver.get('data:,')
    #     driver.get(URL)
    #     driver.switch_to.frame("frame")
    #     input_element = driver.find_elements_by_class_name("form-control")
    #     input_element[0].send_keys(cnpj)
    #     driver.find_element_by_id("btnSubmit").click()
    #     WebDriverWait(driver, 10).until(
    #         ec.presence_of_element_located((By.CLASS_NAME, "panel-body")))

    cnpj_all_infos = driver.find_elements_by_class_name(
        "panel-body")[1].get_attribute('innerText').replace("\n", ":").split(':')
    simples_info = cnpj_all_infos[1].strip()
    mei_info = cnpj_all_infos[3].strip()

    cnpj_resume = {
        "cnpj": cnpj, "situação simples nacional": simples_info, "situação MEI": mei_info}
    cnpj_results.append(cnpj_resume)
    i += 1
    print(f'{i}/{len(CNPJS)}')

    driver.get('data:,')
    driver.get(URL)


keys = cnpj_results[0].keys()
with open('cnpjs_infos.csv', 'w', newline='') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(cnpj_results)
